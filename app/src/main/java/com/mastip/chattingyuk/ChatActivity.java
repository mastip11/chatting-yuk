package com.mastip.chattingyuk;

import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.mastip.chattingyuk.adapter.ChatListAdapter;
import com.mastip.chattingyuk.model.ChatModel;
import com.mastip.chattingyuk.session.UserSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {

    private RecyclerView listChat;
    private FloatingActionButton buttonSend;
    private EditText textInput;

    private EditText editTextUsername;

    private String message;

    private ChatListAdapter adapter;

    private List<ChatModel> list = new ArrayList<>();

    private DatabaseReference databaseReference;
    private Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        initBaseDatabaseReference();
        showUsernameDialog();
        initView();
        settingSendClickListener();
        initListChatAdapter();
    }

    private void initBaseDatabaseReference(){
        databaseReference = FirebaseDatabase.getInstance().getReference();

        query = databaseReference.orderByKey();
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                list.clear();
                Map data = (Map) dataSnapshot.getValue();
                chatParser(data);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void chatParser(Map firebaseData){
//        list.clear();

        Log.v("asd", firebaseData.get("message") + "");

        ChatModel model = new ChatModel();

        model.setMessage(firebaseData.get("message") + "");
        model.setSender(firebaseData.get("sender") + "");
        list.add(model);
//        for (int i = 0; i < firebaseData.size(); i++){
//
//        }

        adapter.notifyDataSetChanged();
    }

    private void initView() {
        listChat = findViewById(R.id.contentChat);
        buttonSend = findViewById(R.id.sendButton);
        textInput = findViewById(R.id.editTextChat);
    }

    private void settingSendClickListener() {
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message = textInput.getText().toString();

                ChatModel chatModel = injectToChatModel(message);
                list.add(chatModel);
                sendToFirebase(chatModel);

                textInput.setText("");

                adapter.notifyDataSetChanged();
            }
        });
    }

    private ChatModel injectToChatModel(String message) {
        ChatModel chatModel = new ChatModel();
        chatModel.setSender(UserSession.getUsername());
        chatModel.setMessage(message);

        return chatModel;
    }

    private void sendToFirebase(ChatModel model) {
        // do action
        FirebaseDatabase.getInstance().getReference().push().setValue(model);
    }

    private void initListChatAdapter() {
        adapter = new ChatListAdapter(this, list);

        listChat.setItemAnimator(new DefaultItemAnimator());
        listChat.setLayoutManager(new LinearLayoutManager(this));
        listChat.setAdapter(adapter);
    }

    private void showUsernameDialog() {
        editTextUsername = new EditText(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Set your username here");
        builder.setView(editTextUsername);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!editTextUsername.getText().toString().equals("")) {
                    UserSession.setName(editTextUsername.getText().toString());
                    UserSession.setUsername(editTextUsername.getText().toString());
                }
            }
        });
        builder.setCancelable(false);
        builder.show();
    }
}
