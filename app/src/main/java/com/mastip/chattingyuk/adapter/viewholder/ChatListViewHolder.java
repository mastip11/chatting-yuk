package com.mastip.chattingyuk.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mastip.chattingyuk.R;
import com.mastip.chattingyuk.model.ChatModel;
import com.mastip.chattingyuk.session.UserSession;

/**
 * Created by HateLogcatError on 11/19/2017.
 */

public class ChatListViewHolder extends RecyclerView.ViewHolder {

    private LinearLayout chatLeftWrapper;
    private LinearLayout chatRightWrapper;
    private TextView chatLeft;
    private TextView chatRight;
    private TextView usernameLeft;
    private TextView usernameRight;

    public ChatListViewHolder(View itemView) {
        super(itemView);

        initView(itemView);
    }

    private void initView(View view) {
        chatLeft = view.findViewById(R.id.chatBubbleLeft);
        chatRight = view.findViewById(R.id.chatBubbleRight);
        chatLeftWrapper = view.findViewById(R.id.leftWrapper);
        chatRightWrapper = view.findViewById(R.id.rightWrapper);
        usernameLeft = view.findViewById(R.id.leftUsernameText);
        usernameRight = view.findViewById(R.id.rightUsernameText);
    }

    public void setUpUI(ChatModel model) {
        if(UserSession.getUsername().equals(model.getSender())) {
            chatRightWrapper.setVisibility(View.VISIBLE);
            chatRight.setText(model.getMessage());
            usernameRight.setText(model.getSender());
        }
        else {
            chatLeftWrapper.setVisibility(View.VISIBLE);
            chatLeft.setText(model.getMessage());
            usernameLeft.setText(model.getSender());
        }
    }
}
