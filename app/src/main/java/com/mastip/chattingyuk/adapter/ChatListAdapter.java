package com.mastip.chattingyuk.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mastip.chattingyuk.R;
import com.mastip.chattingyuk.adapter.viewholder.ChatListViewHolder;
import com.mastip.chattingyuk.model.ChatModel;

import java.util.List;

/**
 * Created by HateLogcatError on 11/19/2017.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListViewHolder> {

    private Context context;
    private List<ChatModel> listChat;

    public ChatListAdapter(Context context, List<ChatModel> listChat) {
        this.context = context;
        this.listChat = listChat;
    }

    @Override
    public ChatListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.chat_bubble, parent, false);
        return new ChatListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatListViewHolder holder, int position) {
        holder.setUpUI(listChat.get(position));
    }

    @Override
    public int getItemCount() {
        return listChat.size();
    }
}
