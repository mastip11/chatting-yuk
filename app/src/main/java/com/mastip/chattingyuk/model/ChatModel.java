package com.mastip.chattingyuk.model;

/**
 * Created by HateLogcatError on 11/19/2017.
 */

public class ChatModel {

    private String sender;
    private String message;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
