package com.mastip.chattingyuk.session;

/**
 * Created by HateLogcatError on 11/19/2017.
 */

public class UserSession {

    private static String username;
    private static String name;

    public static String getUsername() {
        return username;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        UserSession.name = name;
    }

    public static void setUsername(String username) {
        UserSession.username = username;
    }
}
