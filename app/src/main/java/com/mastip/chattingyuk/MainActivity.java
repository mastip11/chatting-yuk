package com.mastip.chattingyuk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mastip.chattingyuk.session.UserSession;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hideActionBar();
        backCounter();

        UserSession.setName("");
        UserSession.setUsername("");
    }

    private void hideActionBar() {
        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    private void backCounter() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(MainActivity.this, ChatActivity.class);
                    startActivity(intent);
                    MainActivity.this.finish();
                }
            }
        }).start();
    }
}
